#
# Cookbook Name:: cookbook-design-gitlab-com
# Recipe:: default
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

include_recipe 'cookbook-design-gitlab-com::nginx'
include_recipe 'cookbook-design-gitlab-com::runner'
