#
# Cookbook Name:: cookbook-about-gitlab-com
# Recipe:: runner
# License:: MIT
#
# Copyright 2017, GitLab Inc.
#

file "/etc/apt/sources.list.d/runner_gitlab-ci-multi-runner.list" do
  content "deb https://packages.gitlab.com/runner/gitlab-runner/#{node['platform']}/ #{node['lsb']['codename']} main\n"
  notifies :run, "bash[setup runner]", :immediately
end

bash "setup runner" do
  code <<-EOH
    curl https://packages.gitlab.com/gpg.key | sudo apt-key add -
    apt-get update
  EOH
  action :nothing
end

package 'gitlab-runner' do
  action [:install,:lock]
end
