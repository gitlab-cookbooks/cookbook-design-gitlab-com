#
# Cookbook Name:: cookbook-design-gitlab-com
# Recipe:: default
# License:: MIT
#
# Copyright 2018, GitLab Inc.
#

include_recipe 'chef-vault'

# Fetch secrets from Chef Vault
design_conf = chef_vault_item(node['cookbook-design-gitlab-com']['chef_vault'],node['cookbook-design-gitlab-com']['vault_item'])

package 'nginx'

template '/etc/nginx/sites-available/design.gitlab.com' do
  source 'nginx.erb'
  mode 0644
  notifies :restart, 'service[nginx]'
end

link '/etc/nginx/sites-enabled/design.gitlab.com' do
  to '/etc/nginx/sites-available/design.gitlab.com'
end

# template '/etc/nginx/sites-available/redirects' do
#   source 'redirects.erb'
#   mode 0644
#   variables({ :redirects => design_conf['redirects'] })
#   notifies :restart, 'service[nginx]'
# end

template '/etc/nginx/cache.conf' do
  source 'cache.conf.erb'
  mode 0644
  notifies :restart, 'service[nginx]'
end

# link '/etc/nginx/sites-enabled/redirects' do
#   to '/etc/nginx/sites-available/redirects'
# end

directory '/home/gitlab-runner/pages/' do
  owner 'gitlab-runner'
  group 'root'
  mode '0755'
  action :create
end

file '/etc/nginx/sites-enabled/default' do
  action :delete
end

directory '/etc/nginx/ssl'

file '/etc/nginx/ssl/design.gitlab.com.crt' do
  mode 0640
  content design_conf['ssl_certificate']
  notifies :restart, 'service[nginx]'
end

file '/etc/nginx/ssl/design.gitlab.com.key' do
  mode 0600
  content design_conf['ssl_key']
  notifies :restart, 'service[nginx]'
end

# design_conf['redirects'].each do | domainname, redirect |
#   file "/etc/nginx/ssl/#{domainname}.crt" do
#     mode 0640
#     content redirect['ssl_certificate']
#     notifies :restart, 'service[nginx]'
#   end

#   file "/etc/nginx/ssl/#{domainname}.key" do
#     mode 0600
#     content redirect['ssl_key']
#     notifies :restart, 'service[nginx]'
#   end
# end

service 'nginx' do
  action :enable
end
