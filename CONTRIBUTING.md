## Contributor license agreement

By submitting code as an individual you agree to the
[individual contributor license agreement](https://docs.gitlab.com/ee/legal/individual_contributor_license_agreement.html).
By submitting code as an entity you agree to the
[corporate contributor license agreement](https://docs.gitlab.com/ee//legal/corporate_contributor_license_agreement.html).
