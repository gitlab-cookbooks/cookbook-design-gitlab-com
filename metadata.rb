name             'cookbook-design-gitlab-com'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Installs/Configures design.gitlab.com'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.6'

depends 'chef-vault', '~> 3.0'
