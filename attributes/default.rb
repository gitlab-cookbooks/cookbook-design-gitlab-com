default['cookbook-design-gitlab-com']['private_token'] = nil

default['cookbook-design-gitlab-com']['ssl_certificate'] = nil
default['cookbook-design-gitlab-com']['ssl_key'] = 'set in chef vault'

default['cookbook-design-gitlab-com']['redirects'] = {}
