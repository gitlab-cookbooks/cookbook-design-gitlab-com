cookbook-about-gitlab-com Cookbook
====================================

Cookbook used to install/configure about.gitlab.com

This cookbook follows the master branch of https://gitlab.com/gitlab-com/about-gitlab-com

Any push to the master branch will be deployed at the next chef run on the node!

To contribute, read [CONTRIBUTING.md](/CONTRIBUTING.md).
